package ru.ilka.tag;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.ilka.entity.Voting;
import ru.ilka.exception.LogicException;
import ru.ilka.logic.VotingLogic;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Here could be your advertisement +375 29 3880490
 */
public class AvaliableVotingsTag extends TagSupport {
    static Logger logger = LogManager.getLogger(AvaliableVotingsTag.class);
    private static final String DATE_TIME_REGEX = "yyyy-MM-dd HH:mm:ss";

    private int accountId;

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    @Override
    public int doStartTag(){
        VotingLogic votingLogic = new VotingLogic();
        ArrayList<Voting> availableVotings = new ArrayList<>();
        try {
            ArrayList<Voting> votings = votingLogic.getAllVotings();
            for (Voting voting:votings) {
                LocalDateTime now = LocalDateTime.now();
                DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_TIME_REGEX);
                String currentTime = now.format(format);
                if(voting.getEndTime().compareTo(currentTime) > 0) {
                    if (votingLogic.checkVotingAvailability(accountId, voting.getVotingId())) {
                        availableVotings.add(voting);
                    }
                }
            }
        } catch (LogicException e) {
            logger.error("Error in AvaliableVotingsTag, can't get all votings " + e);
        }
        if(!availableVotings.isEmpty()) {
            JspWriter out = pageContext.getOut();
            try {
                for (Voting voting:availableVotings) {
                    out.write("<div class=\"buttonCenter\">\n");
                    out.write("<a class=\"button\" style=\"width: 22em;\" href=\"/jsp/user/voting.jsp?votingid="
                            +voting.getVotingId()+"\">" + voting.getName() + "</a>\n");
                    out.write("</div>\n");
                }
            } catch (IOException e) {
                logger.error("Error in AllUsersTag, can't show all accounts " + e);
            }
        }
        return SKIP_BODY;
    }
}

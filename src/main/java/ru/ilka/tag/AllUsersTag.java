package ru.ilka.tag;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.ilka.entity.Account;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * Here could be your advertisement +375 29 3880490
 */
public class AllUsersTag extends TagSupport {
    static Logger logger = LogManager.getLogger(AllUsersTag.class);
    private List<Account> accounts;

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public int doStartTag(){
        JspWriter out = pageContext.getOut();

        try {
            for (int i = 0; i < accounts.size() ; i++) {
                out.write("<tr>");
                Account account = accounts.get(i);
                out.write("<td>" + account.getAccountId() + "</td>");
                out.write("<td>" + account.getLogin() + "</td>");
                out.write("<td>" + account.getFirstName() + "</td>");
                out.write("<td>" + account.getLastName() + "</td>");
                out.write("<td>");
                if(account.isAdmin()){
                    out.write("<input type=\"checkbox\" name=\"" + "admin" + account.getAccountId() + "\" value=\"admin\" checked>");
                }else {
                    out.write("<input type=\"checkbox\" name=\"" + "admin" + account.getAccountId() + "\" value=\"admin\">");
                }
                out.write("</td>");
                out.write("<td>");
                if(account.isBan()){
                    out.write("<input type=\"checkbox\" name=\"" + account.getAccountId() + "\" value=\"ban\" checked>");
                }else {
                    out.write("<input type=\"checkbox\" name=\"" + account.getAccountId() + "\" value=\"ban\">");
                }
                out.write("</td>");
                out.write("</tr>");
            }
        } catch (IOException e) {
            logger.error("Error in AllUsersTag, can't show all accounts " + e);
        }
        return SKIP_BODY;
    }
}

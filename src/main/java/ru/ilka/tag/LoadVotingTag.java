package ru.ilka.tag;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.ilka.entity.Account;
import ru.ilka.entity.Voting;
import ru.ilka.exception.LogicException;
import ru.ilka.logic.AccountLogic;
import ru.ilka.logic.VotingLogic;

import javax.servlet.jsp.tagext.TagSupport;
import java.util.ArrayList;

/**
 * Here could be your advertisement +375 29 3880490
 */
public class LoadVotingTag extends TagSupport {
    static Logger logger = LogManager.getLogger(TagSupport.class);

    private static final String ATTRIBUTE_VOTING = "voting";
    private static final String ATTRIBUTE_CANDIDATES = "candidates";

    private int votingId;

    public void setVotingId(int votingId) {
        this.votingId = votingId;
    }

    @Override
    public int doStartTag() {
        VotingLogic votingLogic = new VotingLogic();
        AccountLogic accountLogic = new AccountLogic();
        try {
            Voting voting = votingLogic.getVoting(votingId);
            ArrayList<Account> candidates = accountLogic.getAllCandidates(votingId);
            pageContext.setAttribute(ATTRIBUTE_VOTING, voting);
            pageContext.setAttribute(ATTRIBUTE_CANDIDATES,candidates);
            return EVAL_PAGE;
        } catch (LogicException e) {
            logger.error("Error while trying to load voting " + e);
        }
        return SKIP_BODY;
    }
}

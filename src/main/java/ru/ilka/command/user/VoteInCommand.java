package ru.ilka.command.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.ilka.command.ActionCommand;
import ru.ilka.entity.Account;
import ru.ilka.exception.CommandException;
import ru.ilka.exception.LogicException;
import ru.ilka.logic.VotingLogic;
import ru.ilka.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static ru.ilka.controller.ControllerConstants.ACCOUNT_KEY;

/**
 * Here could be your advertisement +375 29 3880490
 */
public class VoteInCommand implements ActionCommand {
    static Logger logger = LogManager.getLogger(VoteInCommand.class);

    private static final String PAGE_MAIN = "path.page.main";
    private static final String PARAM_CANDIDATE_ID = "candidateId";
    private static final String PARAM_VOTING_ID = "votingId";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = ConfigurationManager.getProperty(PAGE_MAIN);

        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute(ACCOUNT_KEY);

        int candidateId = Integer.parseInt(request.getParameter(PARAM_CANDIDATE_ID));
        int votingId = Integer.parseInt(request.getParameter(PARAM_VOTING_ID));

        VotingLogic votingLogic = new VotingLogic();
        try {
            votingLogic.voteIn(account.getAccountId(),candidateId,votingId);
        } catch (LogicException e) {
            logger.error("Error in Vote In command" + e);
        }

        return page;
    }
}

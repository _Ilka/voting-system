package ru.ilka.command.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.ilka.command.ActionCommand;
import ru.ilka.command.user.VoteInCommand;
import ru.ilka.entity.Account;
import ru.ilka.exception.CommandException;
import ru.ilka.exception.LogicException;
import ru.ilka.logic.VotingLogic;
import ru.ilka.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



/**
 * Here could be your advertisement +375 29 3880490
 */
public class AddVotingCommand implements ActionCommand {
    static Logger logger = LogManager.getLogger(AddVotingCommand.class);

    private static final String PAGE_MAIN = "path.page.main";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_INFO = "info";
    private static final String PARAM_DURATION = "duration";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = ConfigurationManager.getProperty(PAGE_MAIN);

        int duration = Integer.parseInt(request.getParameter(PARAM_DURATION));
        String name = request.getParameter(PARAM_NAME);
        String info = request.getParameter(PARAM_INFO);

        VotingLogic votingLogic = new VotingLogic();
        try {
            votingLogic.startVoting(name,info,duration*24);
        } catch (LogicException e) {
            throw new CommandException("Can not start new voting " + e);
        }
        return page;
    }
}

package ru.ilka.logic;

import com.sun.org.apache.xerces.internal.impl.xpath.XPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.ilka.dao.StatisticsDao;
import ru.ilka.dao.UsersVotesDao;
import ru.ilka.dao.VotingDao;
import ru.ilka.entity.Voting;
import ru.ilka.exception.DBException;
import ru.ilka.exception.LogicException;

import java.util.ArrayList;

/**
 * Here could be your advertisement +375 29 3880490
 */
public class VotingLogic {
    static Logger logger = LogManager.getLogger(VotingLogic.class);

    public VotingLogic(){

    }

    public void startVoting(String name, String info, int durationInHours) throws LogicException {
        VotingDao votingDao = new VotingDao();
        StatisticsDao statisticsDao = new StatisticsDao();
        try {
            votingDao.addVoting(name,info,durationInHours);
            statisticsDao.addVoting(getVoting(name).getVotingId(),2);
            statisticsDao.addVoting(getVoting(name).getVotingId(),3);
            statisticsDao.addVoting(getVoting(name).getVotingId(),5);
        } catch (DBException e) {
            throw new LogicException("Logic error in starting voting " + e);
        }
    }

    public Voting getVoting(int votingId) throws LogicException {
        VotingDao votingDao = new VotingDao();
        try {
            return votingDao.loadVoting(votingId);
        } catch (DBException e) {
            throw new LogicException("There is no such voting " + e);
        }
    }

    public Voting getVoting(String name) throws LogicException {
        VotingDao votingDao = new VotingDao();
        try {
            return votingDao.loadVoting(name);
        } catch (DBException e) {
            throw new LogicException("There is no such voting " + e);
        }
    }

    public void deleteVoting(int votingId)  throws LogicException {
        VotingDao votingDao = new VotingDao();
        try {
           votingDao.deleteVoting(votingId);
        } catch (DBException e) {
            throw new LogicException("Can not delete voting " + votingId + " " + e);
        }
    }

    public ArrayList<Voting> getAllVotings() throws LogicException {
        VotingDao votingDao = new VotingDao();
        try {
            return votingDao.loadAllVotings();
        } catch (DBException e) {
            throw new LogicException("Can not load all votings " + e);
        }
    }

    public boolean checkVotingAvailability(int accountId, int votingId) throws LogicException{
        UsersVotesDao usersVotesDao = new UsersVotesDao();
        try {
            return  usersVotesDao.isVotingAvaliable(accountId,votingId);
        } catch (DBException e) {
            throw new LogicException("Can not check voting availability " + e);
        }
    }

    public void voteIn(int accountId, int candidateId, int votingId) throws LogicException{
        UsersVotesDao usersVotesDao = new UsersVotesDao();
        StatisticsDao statisticsDao = new StatisticsDao();
        try {
            usersVotesDao.addDecision(accountId, votingId, candidateId);
        } catch (DBException e) {
            throw new LogicException("Can not vote in " + e);
        }
        try {
            statisticsDao.updateStatistics(votingId, candidateId);
        } catch (DBException e) {
            throw new LogicException("Can not update statistics " + e);
        }
    }
}

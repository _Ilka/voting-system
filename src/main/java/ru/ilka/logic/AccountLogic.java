package ru.ilka.logic;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.ilka.comparator.BanComparator;
import ru.ilka.dao.AccountDao;
import ru.ilka.entity.Account;
import ru.ilka.exception.DBException;
import ru.ilka.exception.LogicException;
import ru.ilka.validator.AccountValidator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Here could be your advertisement +375 29 3880490
 */
public class AccountLogic {
    static Logger logger = LogManager.getLogger(AccountLogic.class);

    public AccountLogic() {}

    public boolean checkLogIn(String loginOrEmail, String password) throws LogicException {
        AccountDao accountDao = new AccountDao();
        if (AccountValidator.validateAuthorization(loginOrEmail, password)) {
            try {
                String salt = accountDao.getBirthDateByAuthorization(loginOrEmail);
                password = encodeWithSHA512(password,salt);
            }catch (DBException e){
                return false;
            }
            try {
                return accountDao.authorizeUser(loginOrEmail, password);
            } catch (DBException e) {
                throw new LogicException("Error in account authorization " + e);
            }
        } else {
            return false;
        }
    }

    public LogicResult register(String firstName, String lastName, String email, String login, String password,
                                String birthDate, String avatar, String promises) throws LogicException {
        AccountDao accountDao = new AccountDao();
        LogicResult logicResult;
        if(promises == null){
            promises = "";
        }
        try {
            if (!AccountValidator.validateEmail(email)) {
                logger.debug("invalid email");
                return LogicResult.FAILED;
            }else if (!AccountValidator.validateLogin(login)) {
                logger.debug("invalid log");
                return LogicResult.FAILED;
            } else if (!AccountValidator.validatePassword(password)) {
                logger.debug("invalid pass");
                return LogicResult.FAILED;
            }else if (!accountDao.isEmailUnique(email)) {
                return LogicResult.EMAIL_UNUNIQUE;
            }else if(!accountDao.isLoginUnique(login)){
                return LogicResult.LOGIN_UNUNIQUE;
            }else {
                password = encodeWithSHA512(password,birthDate);
                accountDao.register(firstName,lastName,email,login,password,birthDate,avatar, promises);
                logicResult = LogicResult.OK;
            }
        }catch (DBException e) {
            throw new LogicException("Error in logic while checking register params in database." + e);
        }
        return logicResult;
    }

    public Account getAccountByAuthorization(String loginOrEmail) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            Account account = accountDao.getAccountByAuthorization(loginOrEmail);
            return account;
        } catch (DBException e) {
            throw new LogicException("Error while loading account from database by login or email." + e);
        }
    }

    public Account getAccountByLogin(String login) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            Account account = accountDao.getAccountByLogin(login);
            return account;
        } catch (DBException e) {
            throw new LogicException("Error while loading account from database by login" + e);
        }
    }

    public Account getAccountById(int id) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            Account account = accountDao.getAccountById(id);
            return account;
        } catch (DBException e) {
            throw new LogicException("Error while loading account from database by id " + e);
        }
    }

    public List<Account> getAllAccounts(int accountId) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            List<Account> accounts = accountDao.loadAllAccounts(accountId);
            return accounts;
        } catch (DBException e) {
            throw new LogicException("Error while loading all accounts from database " + e);
        }
    }

    public List<Account> getAllAdmins(int accountId) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            List<Account> accounts = accountDao.loadAllAdmins(accountId);
            return accounts;
        } catch (DBException e) {
            throw new LogicException("Error while loading all admin accounts from database " + e);
        }
    }

    public ArrayList<Account> getAllCandidates(int votingId) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            ArrayList<Integer> candidateIds = accountDao.loadCandidates(votingId);

            if(!candidateIds.isEmpty()) {
                ArrayList<Account> candidates = new ArrayList<>();
                candidateIds.forEach(id -> {
                    try {
                        candidates.add(accountDao.getAccountById(id));
                    } catch (DBException e) {
                        logger.error("Can not load candidate account from database " + e);
                    }
                });
                return candidates;
            }else {
                throw new LogicException("No candidates accounts");
            }
        } catch (DBException e) {
            throw new LogicException("Error while loading candidates accounts from database " + e);
        }
    }

    public void changeFirstName(int accountId, String name) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            accountDao.updateFirstName(accountId,name);
        } catch (DBException e) {
            throw new LogicException("Error while updating firstName" + e);
        }
    }

    public void changeLastName(int accountId, String name) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            accountDao.updateLastName(accountId,name);
        } catch (DBException e) {
            throw new LogicException("Error while updating lastName" + e);
        }
    }

    public LogicResult changeEmail(int accountId, String email) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            if (!AccountValidator.validateEmail(email)) {
                return LogicResult.INVALID_EMAIL;
            } else if (!accountDao.isEmailUnique(email)) {
                return LogicResult.EMAIL_UNUNIQUE;
            } else {
                accountDao.updateEmail(accountId, email);
                return LogicResult.OK;
            }
        } catch (DBException e) {
            throw new LogicException("Error while updating email" + e);
        }
    }

    public LogicResult changeLogin(int accountId, String login) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            if (!AccountValidator.validateLogin(login)) {
                return LogicResult.INVALID_LOGIN;
            } else if (!accountDao.isLoginUnique(login)) {
                return LogicResult.LOGIN_UNUNIQUE;
            } else {
                accountDao.updateLogin(accountId, login);
                return LogicResult.OK;
            }
        } catch (DBException e) {
            throw new LogicException("Error while updating login" + e);
        }
    }

    public LogicResult changePassword(int accountId, String password) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            if (!AccountValidator.validatePassword(password)) {
                return LogicResult.INVALID_PASSWORD;
            }else {
                password = encodeWithSHA512(password, accountDao.getAccountById(accountId).getBirthDate().toString());
                accountDao.updatePassword(accountId, password);
                return LogicResult.OK;
            }
        } catch (DBException e) {
            throw new LogicException("Error while updating password" + e);
        }
    }

    public LogicResult checkPayCard(Account account, String name, int timeLeft){
        String realName = account.getFirstName() + " " + account.getLastName();
        if(!name.toUpperCase().equals(realName.toUpperCase())){
            return LogicResult.WRONG_NAME;
        }else if(timeLeft < 1){
            return LogicResult.UNAVALIABLE_CARD;
        }
        else{
            return LogicResult.OK;
        }
    }

    public void addToBalance(int accountId, BigDecimal amount){

    }

    public void changeBalance(int accountId, BigDecimal balance){

    }

    public void changeAvatar(int accountId, String img) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            accountDao.updateAvatar(accountId,img);
        } catch (DBException e) {
            throw new LogicException("Error while updating avatar" + e);
        }
    }

    public void sortByBanStatus(List<Account> accounts){
        accounts.sort(new BanComparator());
    }

    public void banAccount(int accountId, boolean isBanned) throws LogicException{
        AccountDao accountDao = new AccountDao();
        try {
            accountDao.updateBanStatus(accountId,isBanned);
        } catch (DBException e) {
            throw new LogicException("Error while banning user " + e);
        }
    }

    public void globalBan(List<Account> accounts) throws LogicException {
        AccountDao accountDao = new AccountDao();
        for (Account account : accounts) {
            try {
                accountDao.updateBanStatus(account.getAccountId(),true);
                account.setBan(true);
            } catch (DBException e) {
                throw new LogicException("Error while banning user " + e);
            }
        }
    }

    public void setAdminRole(int accountId, boolean isAdmin) throws LogicException{
        AccountDao accountDao = new AccountDao();
        try {
            accountDao.updateAdminRole(accountId,isAdmin);
        } catch (DBException e) {
            throw new LogicException("Error while updating admin role " + e);
        }
    }

    public int getIdByLogin(String login) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            return accountDao.loadIdByLogin(login);
        } catch (DBException e) {
            throw new LogicException("Can not get accountId by login" + e);
        }
    }

    public boolean checkLoginExistence(String login) throws LogicException {
        AccountDao accountDao = new AccountDao();
        try {
            return !accountDao.isLoginUnique(login);
        } catch (DBException e) {
            throw new LogicException("Error while checking if login " + login + " exists. " + e);
        }
    }



    private String encodeWithSHA512(String data, String salt){
        return DigestUtils.sha512Hex(data + salt);
    }
}

package ru.ilka.logic;

/**
 * Here could be your advertisement +375 29 3880490
 */
public enum LogicResult {
    OK,
    BANNED,
    EMAIL_UNUNIQUE,
    LOGIN_UNUNIQUE,
    REGISTER_INVITE_WRONG,
    FAILED,
    WRONG_NAME,
    UNAVALIABLE_CARD,
    INVALID_PASSWORD,
    INVALID_EMAIL,
    INVALID_LOGIN;
}

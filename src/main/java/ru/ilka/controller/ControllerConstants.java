package ru.ilka.controller;

import java.util.Locale;

/**
 * Here could be your advertisement +375 29 3880490
 */
public class ControllerConstants {
    public static final String VISITOR_KEY = "visitor";
    public static final String ACCOUNT_KEY = "account";
    public static final String SETTINGS_KEY = "settings";
    public static final String IN_GAME_KEY = "inGame";
    public static final String GAME_DEAL_KEY = "deal";
    public static final String CANCEL_TIMER_FIELD = "cancelTimer";
    public static final String DEFAULT_AVATAR_DIRECTORY = "D:\\tomcat\\webapps\\ROOT\\image\\";
    public static final String AVATAR_DIRECTORY_STATIC = "F:\\универ\\3 курс 2 сем\\Курсовая Работа\\voter\\data\\image";
    public static final Locale DEFAULT_LOCALE = new Locale("en", "EN");
    public static final String ONLINE_USERS_KEY = "onlineUsers";
}

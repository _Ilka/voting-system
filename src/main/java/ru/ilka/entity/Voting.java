package ru.ilka.entity;

/**
 * Here could be your advertisement +375 29 3880490
 */
public class Voting {
    private int votingId;
    private String name;
    private String info;
    private String startTime;
    private String endTime;

    public Voting(){

    }

    public Voting(int votingId, String name, String info, String startTime, String endTime) {
        this.votingId = votingId;
        this.name = name;
        this.info = info;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getVotingId() {
        return votingId;
    }

    public void setVotingId(int votingId) {
        this.votingId = votingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Voting voting = (Voting) o;

        if (votingId != voting.votingId) return false;
        if (!name.equals(voting.name)) return false;
        if (info != null ? !info.equals(voting.info) : voting.info != null) return false;
        if (!startTime.equals(voting.startTime)) return false;
        return endTime.equals(voting.endTime);
    }

    @Override
    public int hashCode() {
        int result = votingId;
        result = 31 * result + name.hashCode();
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + startTime.hashCode();
        result = 31 * result + endTime.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Voting{" +
                "votingId=" + votingId +
                ", name='" + name + '\'' +
                ", info='" + info + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                '}';
    }
}

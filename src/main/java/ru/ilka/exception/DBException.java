package ru.ilka.exception;

/**
 * Here could be your advertisement +375 29 3880490
 */
public class DBException extends Exception {
    public DBException() {
    }

    public DBException(String message) {
        super(message);
    }

    public DBException(String message, Throwable cause) {
        super(message, cause);
    }

    public DBException(Throwable cause) {
        super(cause);
    }
}

package ru.ilka.dao;

import ru.ilka.datebase.ConnectionPool;
import ru.ilka.exception.DBException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Here could be your advertisement +375 29 3880490
 */
public class StatisticsDao {
    private static final String UPDATE_STATISTICS = "UPDATE `statistics` SET `votes`= `votes` + 1 WHERE `voting_id`= ? AND `candidate_id` = ?";
    private static final String ADD_VOTING = "INSERT INTO `statistics` (`voting_id`, `candidate_id`) VALUES (?, ?);";

    public StatisticsDao(){

    }

    public void updateStatistics(int votingId, int candidateId) throws DBException {
        try(Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_STATISTICS)) {
            preparedStatement.setInt(1,votingId);
            preparedStatement.setInt(2,candidateId);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DBException("Error while updating statistics " + e);
        }
    }

    public void addVoting(int votingId, int candidateId) throws DBException {
        try(Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ADD_VOTING)) {
            preparedStatement.setInt(1,votingId);
            preparedStatement.setInt(2,candidateId);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DBException("Error while updating statistics " + e);
        }
    }
}

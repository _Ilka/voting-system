package ru.ilka.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.ilka.datebase.ConnectionPool;
import ru.ilka.exception.DBException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Here could be your advertisement +375 29 3880490
 */
public class UsersVotesDao {
    static Logger logger = LogManager.getLogger(UsersVotesDao.class);

    private static final String FIND_DECISION_IN_VOTING = "SELECT `decision_id` FROM `users_votes` WHERE `where_id` = ? AND `user_id` = ?";
    private static final String ADD_DECISION = "INSERT INTO `users_votes` (`user_id`, `where_id`, `selected_candidate_id`) VALUES (?, ?, ?)";

    public void addDecision(int accountId, int votingId, int candidateId) throws DBException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_DECISION)) {
            preparedStatement.setInt(1,accountId);
            preparedStatement.setInt(2,votingId);
            preparedStatement.setInt(3,candidateId);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DBException("Error while inserting new user decision into database." + e);
        }
    }

    public boolean isVotingAvaliable(int accountId, int votingId) throws DBException {
        try(Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_DECISION_IN_VOTING)){
            preparedStatement.setInt(1,votingId);
            preparedStatement.setInt(2,accountId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return  !resultSet.next();
        } catch (SQLException e) {
            throw new DBException("Error while checking voting availability in database." + e);
        }
    }
}

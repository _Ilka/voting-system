package ru.ilka.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.ilka.datebase.ConnectionPool;
import ru.ilka.entity.Voting;
import ru.ilka.exception.DBException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Here could be your advertisement +375 29 3880490
 */
public class VotingDao {
    static Logger logger = LogManager.getLogger(VotingDao.class);

    private static final String FIND_VOTING_BY_ID = "SELECT `voting_id`, `info`, `name`, `start_time`, `end_time` FROM `votings` WHERE `voting_id` = ?";
    private static final String FIND_VOTING_BY_NAME = "SELECT `voting_id`, `info`, `name`, `start_time`, `end_time` FROM `votings` WHERE `name` = ?";
    private static final String ADD_VOTING = "INSERT INTO `votings` (`name`, `info`, `end_time`) VALUES (?, ?, DATE_ADD(CURRENT_TIMESTAMP, INTERVAL ? HOUR));";
    private static final String DELETE_VOTING = "DELETE FROM `votings` WHERE `voting_id`= ?";
    private static final String LOAD_ALL_VOTINGS = "SELECT `voting_id`, `info`, `name`, `start_time`, `end_time` FROM `votings`";

    private static final String COLUMN_VOTING_ID = "voting_id";
    private static final String COLUMN_INFO = "info";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_START_TIME = "start_time";
    private static final String COLUMN_END_TIME = "end_time";

    public VotingDao(){

    }

    public void addVoting(String name, String info, int durationInHours) throws DBException {
        try(Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ADD_VOTING)){
            preparedStatement.setString(1,name);
            preparedStatement.setString(2,info);
            preparedStatement.setInt(3,durationInHours);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DBException("Error while inserting new voting into database." + e);
        }
    }

    public Voting loadVoting(int votingId) throws DBException {
        try(Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_VOTING_BY_ID)){
            preparedStatement.setInt(1,votingId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                Voting voting = new Voting();
                voting.setVotingId(resultSet.getInt(COLUMN_VOTING_ID));
                voting.setName(resultSet.getString(COLUMN_NAME));
                voting.setInfo(resultSet.getString(COLUMN_INFO));
                voting.setStartTime(resultSet.getString(COLUMN_START_TIME));
                voting.setEndTime(resultSet.getString(COLUMN_END_TIME));
                return voting;
            }else {
                throw new DBException("There is no voting with Id = " + votingId);
            }
        } catch (SQLException e) {
            throw new DBException("Error while loading  voting by Id" + e);
        }
    }

    public Voting loadVoting(String name) throws DBException {
        try(Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_VOTING_BY_NAME)){
            preparedStatement.setString(1,name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                Voting voting = new Voting();
                voting.setVotingId(resultSet.getInt(COLUMN_VOTING_ID));
                voting.setName(resultSet.getString(COLUMN_NAME));
                voting.setInfo(resultSet.getString(COLUMN_INFO));
                voting.setStartTime(resultSet.getString(COLUMN_START_TIME));
                voting.setEndTime(resultSet.getString(COLUMN_END_TIME));
                return voting;
            }else {
                throw new DBException("There is no voting with such name");
            }
        } catch (SQLException e) {
            throw new DBException("Error while loading  voting by name" + e);
        }
    }

    public ArrayList<Voting> loadAllVotings() throws DBException {
        try(Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(LOAD_ALL_VOTINGS)){
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Voting> votings = new ArrayList<>();
            while (resultSet.next()) {
                Voting voting = new Voting();
                voting.setVotingId(resultSet.getInt(COLUMN_VOTING_ID));
                voting.setName(resultSet.getString(COLUMN_NAME));
                voting.setInfo(resultSet.getString(COLUMN_INFO));
                voting.setStartTime(resultSet.getString(COLUMN_START_TIME));
                voting.setEndTime(resultSet.getString(COLUMN_END_TIME));
                votings.add(voting);
            }
            return votings;
        } catch (SQLException e) {
            throw new DBException("Error while loading all votings " + e);
        }
    }

    public void deleteVoting(int votingId) throws DBException{
        try(Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_VOTING)){
            preparedStatement.setInt(1,votingId);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DBException("Error while deleting voting" + e);
        }
    }
}

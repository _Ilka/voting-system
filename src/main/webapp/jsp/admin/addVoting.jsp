<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="voter-tags" uri="http://ilka.ru/votertags/" %>
<fmt:setLocale value="${visitor.locale}" scope="session"/>
<fmt:setBundle basename="properties.content"/>
<jsp:useBean id="accounts" class="ru.ilka.entity.Account" scope="page"/>
<c:set var="context" scope="page" value="${pageContext.request.contextPath}"/>

<html>
<head>
    <title><fmt:message key="application.title"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/css" rel="stylesheet" href="${context}/css/common.css"/>
    <link type="text/css" rel="stylesheet" href="${context}/css/login_stylesheet.css"/>
    <%-- icon --%>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/image/vote_icon.png"/>
    <script src="${context}/js/common_scr.js"></script>
</head>
<body>
<c:choose>
    <c:when test="${visitor.role eq 'GUEST'}">
        <jsp:forward page="/jsp/guest/start.jsp"/>
    </c:when>
    <c:when test="${visitor.role eq 'USER'}">
        <jsp:forward page="/jsp/user/main.jsp"/>
    </c:when>
</c:choose>

<%-- Header --%>
<c:import url="${context}/WEB-INF/jspf/user_header.jsp"/>

<div class="q"></div>
<div class="row">
    <div class="col-1"></div>
    <div class="col-10">
        <div class = "description">
            <form name="addVotingForm" method="POST" action="/controller">
                <input type="hidden" name="command" value="addVoting"/>
                <table>
                    <tr class="bordered">
                        <td>Title*</td>
                        <td>
                            <input type="text" name="name"  autofocus required maxlength=45>
                        </td>
                    </tr>
                    <tr class="bordered">
                        <td>Description*</td>
                        <td>
                            <input type="text" name="info" required maxlength=500>
                        </td>
                    </tr>
                    <tr class="bordered">
                        <td>Duration in days*</td>
                        <td>
                            <input type="number" name="duration" min="1" required>
                        </td>
                    </tr>
                    <tr>
                        <td align=center colspan=2>
                            <input class="button" type="submit" value="Add">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    <div class="col-1"></div>
</div>

<%-- Footer --%>
<div class="row">
    <c:set var="path" value="path.page.users" scope="session"/>
    <c:import url="${context}/WEB-INF/jspf/footer.jsp"/>
</div>
</body>

</html>



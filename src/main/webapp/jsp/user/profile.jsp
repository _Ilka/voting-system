<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="voter-tags" uri="http://ilka.ru/votertags/" %>
<fmt:setLocale value="${visitor.locale}" scope="session"/>
<fmt:setBundle basename="properties.content"/>
<jsp:useBean id="account" class="ru.ilka.entity.Account" scope="session" />
<jsp:useBean id="receivedText" class="java.lang.String" scope="page"/>
<jsp:useBean id="receivedFrom" class="java.lang.String" scope="page"/>
<c:set var="context" scope="page" value="${pageContext.request.contextPath}"/>

<html>
<head>
    <title><fmt:message key="application.title"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/css" rel="stylesheet" href="${context}/css/common.css"/>
    <link type="text/css" rel="stylesheet" href="${context}/css/profilepage.css"/>
    <%-- icon --%>
    <link rel="shortcut icon" href="${context}/image/vote_icon.png"/>
    <script src="${context}/js/common_scr.js"></script>
    <script src="${context}/js/profile_scr.js"></script>
</head>

<body>
    <c:choose>
        <c:when test="${visitor.role eq 'GUEST'}">
            <jsp:forward page="/jsp/guest/start.jsp"/>
        </c:when>
    </c:choose>

    <%-- Header --%>
    <c:import url="${context}/WEB-INF/jspf/user_header.jsp"/>

    <div class="q"></div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-7">
            <div class = "description">
                <h3><fmt:message key="profile.h3.myProfile"/></h3>
                <div class="container">
                    <c:choose>
                        <c:when test="${account.avatar eq 'default_avatar.jpg'}">
                            <img src="${context}/image/default_avatar.jpg" alt="Avatar1">
                        </c:when>
                        <c:otherwise>
                            <img src="/imageLoader?avatar=${account.avatar}" alt="Avatar2">
                        </c:otherwise>
                    </c:choose>
                    <form class="newImg" name="avatarForm" enctype="multipart/form-data" method="POST" action="/controller">
                        <input type="hidden" name="command" value="changeAvatar"/>
                        <input  type="file" name="avatar" accept="image/jpeg">
                        <div class="submit">
                            <input class="button" type="submit" value="<fmt:message key="profile.button.save"/>">
                        </div>
                    </form>
                </div>
                <div class="info">
                        <form name="profileInfoForm" onsubmit="return validateForm()"  method="POST" action="/controller" autocomplete="on">
                            <input type="hidden" name="command" value="info"/>
                            <span class="PasswordMessage">${successPasswordChangeMessage}</span>
                            <span class="ErrorPasswordMessage">${errorPasswordChangeMessage}</span>
                            <table>
                                <tr class="inf">
                                    <td>
                                        <span class="infTitle"><fmt:message key="profile.firstName"/></span>
                                        <span class="successMessage">${successNameChangeMessage}</span>
                                    </td>
                                </tr>
                                <tr class="bordered">
                                    <td class="fullInput">
                                        <input type="text" name="firstName" value="${account.firstName}" pattern="[a-zA-Z]+" required maxlength=30>
                                    </td>
                                </tr>

                                <tr class="inf">
                                    <td>
                                        <span class="infTitle"><fmt:message key="profile.lastName"/></span>
                                        <span class="successMessage">${successLastNameChangeMessage}</span>
                                    </td>
                                </tr>
                                <tr class="bordered">
                                    <td class="fullInput">
                                        <input type="text" name="lastName" value="${account.lastName}" pattern="[a-zA-Z]+([ ]{1}[a-zA-Z]+)*" required maxlength=30>
                                    </td>
                                </tr>

                                <tr class="inf">
                                    <td>
                                        <span class="infTitle"><fmt:message key="profile.email"/></span>
                                        <span class="successMessage">${successEmailChangeMessage}</span>
                                        <span class="errorMessage">${errorEmailChangeMessage}</span>
                                    </td>
                                </tr>
                                <tr class="bordered">
                                    <td class="fullInput">
                                        <input type="email" name="email" value="${account.email}" pattern="^([a-zA-Z0-9_-]+\.)*[a-zA-Z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$"  title="<fmt:message key="register.email.title"/>" required  maxlength=50>
                                    </td>
                                </tr>

                                <tr class="inf">
                                    <td>
                                        <span class="infTitle"><fmt:message key="profile.login"/></span>
                                        <span class="successMessage">${successLoginChangeMessage}</span>
                                        <span class="errorMessage">${errorLoginChangeMessage}</span>
                                    </td>
                                </tr>
                                <tr class="bordered">
                                    <td class="fullInput">
                                        <input type="text" name="login" value="${account.login}" pattern="[a-zA-Z0-9_]{4,50}" title="<fmt:message key="register.userName.title"/>" required  maxlength=45>
                                    </td>
                                </tr>

                                <tr class="inf">
                                    <td>
                                        <span id="passSpan1" class="infTitle" style="display:none;"><fmt:message key="profile.password"/></span>
                                    </td>
                                </tr>
                                <tr class="bordered">
                                    <td id="password1" class="fullInput"></td>
                                    <td>
                                        <span class="error" id="err-pwd1"></span>
                                    </td>
                                </tr>

                                <tr class="inf">
                                    <td>
                                        <span id="passSpan2" class="infTitle" style="display:none;"><fmt:message key="profile.password2"/></span>
                                    </td>
                                </tr>
                                <tr class="bordered">
                                    <td id="password2" class="fullInput"></td>
                                    <td>
                                        <span class="error" id="err-pwd2"></span>
                                    </td>
                                </tr>

                                <tr class="inf">
                                    <td>
                                        <span class="infTitle"><fmt:message key="profile.date"/></span>
                                    </td>
                                </tr>
                                <tr class="bordered">
                                    <td class="fullInput" >
                                        <input type="date" name="birthDate" value="${account.birthDate}" readonly/>
                                    </td>
                                </tr>

                                <tr class="inf">
                                    <td>
                                        <span class="infTitle"><fmt:message key="profile.promises"/></span>
                                    </td>
                                </tr>
                                <tr class="bordered">
                                    <td class="fullInput">
                                        <input name="promises" value="${account.promises}" readonly/>
                                    </td>
                                </tr>

                                <tr class ="submit">
                                    <td colspan="2">
                                        <input id="buttonPassword" class="button" type="button" value="<fmt:message key="profile.button.changePass"/>" onclick="showPassword()">
                                        <input class="button" type="submit" value="<fmt:message key="profile.button.save"/>">
                                    </td>
                                </tr>
                            </table>
                        </form>
                </div>
            </div>
        </div>
        <div class="col-3">
            <voter-tags:new-messages accountId="${account.accountId}">
                <div class="description" id="newMessageReceived">
                    <div class="newMessageLabel"><h3><fmt:message key="profile.newmessage.label"/></h3></div>
                    <div class="messageRow">
                        <button class="button" onclick="newMessageModal.style.display='block';" id="new"><fmt:message key="profile.newmessage.btn.show"/></button>
                    </div>
                </div>
            </voter-tags:new-messages>
            <div id="id01" class="mask">
                <div class="modal animate">
                    <span onclick="document.getElementById('id01').style.display='none'" class="close" title="<fmt:message key="login.close.title"/>">&times;</span>
                    <div class="newMessage">
                        <div class="messageRow">
                            <fmt:message key="profile.newmessage.from"/> ${receivedFrom}
                        </div>
                        <div class="messageNewText">
                            <textarea id="inputText" readonly>${receivedText}</textarea>
                        </div>
                        <div class="messageRow" style="display: flex; justify-content: space-around">
                            <form name="deleteMessageForm" method="POST" action="/controller" autocomplete="on">
                                <input type="hidden" name="command" value="deleteMessage"/>
                                <input class="button" type="submit" value="<fmt:message key="profile.newmessage.btn.delete"/>"/>
                            </form>
                            <form name="markMessageForm" method="POST" action="/controller" autocomplete="on">
                                <input type="hidden" name="command" value="markMessage"/>
                                <input class="button" type="submit" title="<fmt:message key="profile.newmessage.btn.read.title"/>" value="<fmt:message key="profile.newmessage.btn.read"/>"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-1"></div>
    </div>

    <%-- Footer --%>
    <div class="row">
        <c:set var="path" value="path.page.profile" scope="session"/>
        <c:import url="${context}/WEB-INF/jspf/footer.jsp"/>
    </div>
    <script>newMessageModal = document.getElementById('id01');</script>
    <script>closeModal();</script>
</body>

</html>


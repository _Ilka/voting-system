<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="voter-tags" uri="http://ilka.ru/votertags/" %>
<fmt:setLocale value="${visitor.locale}" scope="session"/>
<fmt:setBundle basename="properties.content"/>
<jsp:useBean id="account" class="ru.ilka.entity.Account" scope="session" />
<jsp:useBean id="voting" class="ru.ilka.entity.Voting" scope="page" />
<jsp:useBean id="candidates" class="ru.ilka.entity.Account" scope="page" />
<c:set var="context" scope="page" value="${pageContext.request.contextPath}"/>
<c:set var="paramVotingId" scope="page" value="votingid"/>
<c:set var="currentVotingId" scope="page" value="${pageContext.request.getParameter(paramVotingId)}"/>
<voter-tags:load-voting votingId="${pageContext.request.getParameter(paramVotingId)}"></voter-tags:load-voting>

<html>
<head>
    <title><fmt:message key="application.title"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/css" rel="stylesheet" href="${context}/css/common.css"/>
    <link type="text/css" rel="stylesheet" href="${context}/css/voting.css"/>
    <%-- icon --%>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/image/vote_icon.png"/>
    <script src="${context}/js/common_scr.js"></script>
</head>
<body>
    <c:choose>
        <c:when test="${visitor.role eq 'GUEST'}">
            <jsp:forward page="/jsp/guest/start.jsp"/>
        </c:when>
    </c:choose>

    <%-- Header --%>
    <c:import url="${context}/WEB-INF/jspf/user_header.jsp"/>

    <div class="q"></div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-10">
            <div class = "description">
                <div class="votingName">${voting.name}</div>
                <div class="votingInfo">${voting.info}</div>
                <c:forEach items="${candidates}" var="candidate">
                    <div class="candidate">
                        <div class="container">
                            <c:choose>
                                <c:when test="${candidate.avatar eq 'default_avatar.jpg'}">
                                    <img src="${context}/image/default_avatar.jpg" alt="Avatar1">
                                </c:when>
                                <c:otherwise>
                                    <img src="/imageLoader?avatar=${candidate.avatar}" alt="Avatar2">
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="candidateInfo">
                            <div>${candidate.firstName} ${candidate.lastName}</div>
                            <div>birth date: ${candidate.birthDate}</div>
                            <div>promises: ${candidate.promises}</div>
                        </div>
                        <div class="fieldForm">
                            <form method="POST" action="/controller" autocomplete="on">
                                <input type="hidden" name="command" value="voteIn"/>
                                <input type="hidden" name="candidateId" value="${candidate.accountId}"/>
                                <input type="hidden" name="votingId" value="${currentVotingId}"/>
                                <input class="field" type="submit" value=""/>
                            </form>
                        </div>
                    </div>
                </c:forEach>
                <div class="candidate">
                    <div class="container">
                        <img src="${context}/image/default_avatar.jpg" alt="Avatar1"/>
                    </div>
                    <div class="candidateInfo">
                        <div>Against Everybody</div>
                    </div>
                    <div class="fieldForm">
                        <form method="POST" action="/controller" autocomplete="on">
                            <input type="hidden" name="command" value="voteIn"/>
                            <input type="hidden" name="candidateId" value="5"/>
                            <input type="hidden" name="votingId" value="${currentVotingId}"/>
                            <input class="field" type="submit" value=""/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-1">
        </div>
    </div>
    <%-- Footer --%>
    <div class="row">
        <c:set var="path" value="path.page.voting" scope="session"/>
        <c:import url="${context}/WEB-INF/jspf/footer.jsp"/>
    </div>
</body>

</html>
